[BITS 16]
[ORG 0x7c00]
;load a hello world from the next place on disk
mov ax, 0xC0
mov es, ax
mov ah, 2
mov al, 1
mov cl, 2
mov ch, 0
mov dh, 0
;dl is already set
;es:bx is our pointer
xor bx, bx
int 13h

mov ax, 0x8000
mov es, ax
mov ah, 2
mov al, 1 ;our kernel is one sector right now, we want two
mov cl, 3
mov ch, 0
mov dh, 0
;dl is already set
;es:bx is our pointer
xor bx, bx
int 13h

mov ah, 0
mov al, 11h
int 10h

;call nmi_disable ;even the non-maskable ones
;enable the a20 line
call a20enable
cli ;clear intterupts
;load the GLOBAL DESCRIPTOR TABLE [OH, JOY]
xor ax,ax
mov ds, ax ;<< correctly address GDT
lgdt [gdt.ptr]

;enable protected mode
mov eax, cr0
or eax, 1
mov cr0, eax

;right. now we need to clear the pipe of 16 bit instructions.
;we can do that by jumping

jmp 08h:magic32happyland
nop
nop
nop ;extra padding just cause.

;%include "nmidisable.asm"
%include "a20utils.asm"
%include "gdt32.asm"

[BITS 32]
magic32happyland:
;set up the stack.
mov ax, 0x10
mov ds, ax
mov es, ax
mov gs, ax

mov byte [ebx],'G' ;DEBUG
;do something
kmain:
jmp 08h:0xC00


boot:
TIMES 510-($-$$) db 0
dw 0xAA55
