all:
	nasm -f bin stage1.asm
	nasm -f bin stage2.asm
	nasm -f bin kernel.asm
	cat stage1 stage2 kernel | dd of=test.img
qemu:
	qemu-system-x86_64 test.img
