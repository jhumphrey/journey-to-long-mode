[BITS 32]
[ORG 0xC00] 
landing:
;SET UP THE STACK
mov ax, 0x10
mov ds, ax
mov es, ax
mov fs, ax
mov gs, ax
mov ss, ax
;Do we need to flip the direction bit?

;DEBUG: prove we worked by printing PM to screen
mov ebx, 0xb8000
mov dword [ebx], "P0M0"
;jmp $ ;DEBUG

;SET UP THE PML4E
%define PML4E_BASE 0x1000
%define PDPE_BASE 0x2000
%define PDE_BASE 0x3000
%define PTE_BASE 0x4000

pml4e_init:
mov edi, PML4E_BASE
mov eax, 0x00000007 & (PDPE_BASE << 12)
stosd
add edi, 4
xor eax, eax
mov ecx, 6 ;3 qword entries = 6 dword ones
rep stosd

pdpe_init:
mov edi, PDPE_BASE
mov eax, 0x00000007 & (PDE_BASE << 12)
stosd
add edi, 4
xor eax, eax
mov ecx, 0xFFF << 1 
rep stosd

pde_init:
mov edi, PDE_BASE
mov eax, 0x00000007 & (PTE_BASE << 12)
stosd
add edi, 4
xor eax, eax
mov ecx, 0xFFF << 1 
rep stosd

pte_init:
mov edi, PTE_BASE
mov eax, 0x00000087
mov ecx, 512
.tiler:
	stosd
	add edi, 4
	add eax, 0x1000
loop .tiler

;Now set the cr3 register to the address
mov eax, PML4E_BASE
mov cr3, eax

;Now we want to turn on long mode via EFER_MSR
mov ecx, 0xC0000080
rdmsr
or eax, 0x100
wrmsr

;now we want to enable paging
mov eax, cr0
or eax, (1<<31)
mov cr0, eax

;now we want to jmp to our kernel located at 0x9000
;jmp 0x8000

jmp lminit
nop
[BITS 64]
lminit:
mov rax, 0xFFFFFFFFFFFFFFFF
mov edi, 0xA0000
;stosq
mov qword [edi], rax
jmp $
gdt:
.code:
dq 0x00209A0000000000
.data:
dq 0x0020920000000000
.ptr:
dw $-gdt-1
dq gdt

times 510-($-$$) db 0
dw 0xBEEF ;DEBUG SIGNATURE FOR SECOND STAGE :)
