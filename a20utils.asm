;FUNCTION TO TEST THE A20 LINE
;method based on: http://www.independent-software.com/operating-system-development-enabling-a20-line.html

a20enable:
	push ax
		call a20test
		call a20bios
		call a20test
		call a20kbd
		call a20test
		call a20fast
		call a20test
;		hlt ;it failed.
	.enabled:
	pop ax ;extra pop for IP
	pop ax
ret

a20test:
	mov ax, 0x0000
	mov es, ax
	mov byte [ES:0x500], 0x00
	mov ax, 0xFFFF
	mov es, ax
mov byte [ebx], 0xFE
	mov byte [ES:0x510], 0xFF
mov byte [ebx], 0xFD
	mov ax, 0x0000
	mov es, ax
	mov al, byte [ES:0x500]
mov byte [ebx], 0x3
	;if al is 0 then a20 is enabled
	cmp al, 0 ;TODO: can we use some nifty or operation?
	je a20enable.enabled
mov byte [ebx], 0x2
ret

a20bios:
	;NOTE this was copy pasted, I am getting to much like Duct von Tape!
	sti
	mov ax, 0x2401
	int 15h
	cli
ret

a20kbd:
	mov byte [ebx], 0xFF
	;DISABLE KEYBOARD
	call kbdcmdwait
	mov al, 0xad ;DISABLE ps2 port one (kbd)
	out 0x64, al

	call kbdcmdwait
	mov al, 0xD0 ;CTR_OUT READ
	out 0x64, al

	call kbddatawait
	in al, 0x60
	or al, 2	;a20 enable
	out 0x60, al	

	call kbdcmdwait
	mov al, 0xD1 ;CTR_OUT WRITE
	out 0x64, al

	;ENABLE KEYBOARD
	call kbdcmdwait
	mov al, 0xae ;enable ps2 1
	out 0x64, al
ret

kbdcmdwait:
	;This one was also copied :/
	in al, 0x64	
	test al, 2	;BIT b10: INBUF_STAT
	jnz kbdcmdwait
ret

kbddatawait:
	in al, 0x64
	test al, 1 ;OUTBUF_STAT
	jnz kbddatawait
ret

a20fast:
	;PERISH ALL HOPE YE WHO ENTER HERE
	;AGAIN THIS IS COPIED :/
	in al, 0x92
	or al, 2
	out 0x92, al
ret
