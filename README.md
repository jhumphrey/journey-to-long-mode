# Journey to Long Mode

This is my two (or three) stage bootloader for amd64.
I used osdev wiki, the AMD manual, and https://www.youtube.com/watch?v=FzvDGDdtzws

#build
```sh
make
make qemu
```
# How does it work?
Stage 1 loads stage 2 into memeory, as well as the kernel.
Stage 1 goes into protected Mode
stage 2 goes into long Mode
and then jumps to the kernel which is at ORG 0x8000
