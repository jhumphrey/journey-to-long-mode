gdt:
	.null:
		dq 0
	.code:
		;00h-0Fh: Limit 
		;10h-1Fh: Base 
		;20h-27h: Base 
		;28h-2Fh: Access Byte
			;28h: AC
			;29h: RW
			;2Ah: DC
			;2Bh: Ex
			;2Ch: S
			;2D+: PRIV
			;2F: Present
		;=b10011110 ;I don't understand why S is 0
		;the docs say S should be set for code segs
		;30h-33h: Limit
		;34h-37h: FLAGS
			;36h: Sz
			;37h: Gr
			;=1100
		;38-3Fh: base
		;dq 0x00CF9E000000FFFF
		dq 0x00CF9A000000FFFF
	.data:
		;same, except the stack goes up
		;bit 2Ah is 0
		;dq 0x00CF9A000000FFFF
		dq 0x00CF92000000FFFF
		
.ptr:
	dw 24
	dd gdt
